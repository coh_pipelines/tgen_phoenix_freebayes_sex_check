ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu19_opt_freebayes:latest as opt_freebayes
FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu19_opt_bcftools:1.10.2 as opt_bcftools
FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu19_opt_samtools:1.10 as opt_samtools

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu19_base

RUN apt update -y && \
    apt install -y bc gawk bash && \
    apt autoremove -y && \
    apt clean

COPY --from=opt_freebayes /opt /opt
COPY --from=opt_bcftools /opt /opt
COPY --from=opt_samtools /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /opt/bin/ && echo "#!/bin/bash" > /opt/bin/module && chmod a+x /opt/bin/module